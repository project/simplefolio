<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>">
<head>

  <title><?php print $head_title; ?></title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $head; ?>
  <?php print $styles; ?>

</head>
<body class="<?php print $body_classes; ?>">

  <div id="page">
    <div id="header" class="clear-block">

      <div id="logo-title" class="clear-block">
        <?php if (!empty($logo)): ?>
          <a id="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

        <div id="title">
          <?php if (!empty($site_name)): ?>
            <h1 id="site-name">
              <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
            </h1>
          <?php endif; ?>

          <?php if (!empty($site_slogan)): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
        </div> <!-- /title -->
      </div> <!-- /logo-title -->

      <?php if (!empty($header) || !empty($primary_links)): ?>
        <div id="header-right">
          <?php if (!empty($header)): ?>
            <div id="header-region" class="region clear-block"><?php print $header; ?></div>
          <?php endif; ?>

          <?php if (!empty($primary_links)): ?>
            <div id="primary" class="clear-block"><?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?></div>
          <?php endif; ?>
        </div> <!-- /header-right -->
      <?php endif; ?>

    </div> <!-- /header -->
    <div id="container" class="clear-block">
      <div id="content"<?php if (!empty($content_class)) print ' class="'. $content_class .'"'; ?>><div id="content-inner">

        <?php if (!empty($mission)): ?>
          <div id="mission"><?php print $mission; ?></div>
        <?php endif; ?>

        <?php if (!empty($title)): ?>
          <h2 id="page-title"><?php print $title; ?></h2>
        <?php endif; ?>

        <?php if (!empty($tabs)): ?>
          <div class="tabs"><?php print $tabs; ?></div>
        <?php endif; ?>

        <?php print $messages; ?>

        <?php print $help; ?>

        <?php print $content; ?>

        <?php if (!empty($feed_icons)): ?>
          <div class="feed-icon"><?php print $feed_icons; ?></div>
        <?php endif; ?>

      </div></div> <!-- /content-inner /content -->

      <?php if (!empty($search_box) || !empty($secondary_links) || !empty($right)): ?>
        <div id="right" class="sidebar">

          <?php if (!empty($search_box)): ?>
            <div id="search-box"><?php print $search_box; ?></div>
          <?php endif; ?>

          <?php if (!empty($secondary_links)): ?>
            <div id="secondary"><?php print theme('links', $secondary_links, array('class' => 'links secondary-links')); ?></div>
          <?php endif; ?>

          <?php if (!empty($right)): ?>
            <div id="right-region" class="region clear-block"><?php print $right; ?></div>
          <?php endif; ?>

        </div> <!-- /right -->
      <?php endif; ?>

    </div> <!-- /container -->
    <div id="footer" class="clear-block">

      <?php if (!empty($footer)): ?>
        <div id="footer-region" class="region clear-block"><?php print $footer; ?></div>
      <?php endif; ?>

      <?php if (!empty($primary_links)): ?>
        <div id="primary-footer" class="clear-block"><?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?></div>
      <?php endif; ?>

      <?php if (!empty($footer_message)): ?>
        <div id="footer-message"><?php print $footer_message; ?></div>
      <?php endif; ?>

    </div> <!-- /footer -->
  </div> <!-- /page -->

  <?php print $scripts; ?>
  <?php print $closure; ?>

</body>
</html>
