<div id="node-<?php print $node->nid; ?>" class="node <?php print $node->type; ?><?php if (!empty($sticky)) print ' sticky'; ?><?php if (empty($status)) print ' node-unpublished'; ?> clear-block">

  <?php if (empty($page)): ?>
    <?php if (module_exists('comment') && in_array($node->comment, array(COMMENT_NODE_READ_ONLY, COMMENT_NODE_READ_WRITE))): ?>
      <?php
        // If comments enabled, display comment count next to title
        $comment_count = '';
        $comments = comment_link('node', $node, TRUE);
        if (!empty($comments['comment_comments'])) {
          $number_comments = comment_num_all($node->nid);
          $comment_count = l($number_comments[0], $comments['comment_comments']['href'], array('attributes' => $comments['comment_comments']['attributes'], 'fragment' => $comments['comment_comments']['fragment']));
        }
        elseif (!empty($comments['comment_add'])) {
          $comment_count = l('0', $comments['comment_add']['href'], array('attributes' => $comments['comment_add']['attributes'], 'fragment' => $comments['comment_add']['fragment']));
        }
        else {
          $comment_count = '0';
        }
      ?>
      <span class="comment-count"><?php print $comment_count; ?></span>
    <?php endif; ?>

    <h3><a href="<?php print $node_url; ?>" title="<?php print $title; ?>"><?php print $title; ?></a></h3>
  <?php endif; ?>

  <?php if (!empty($submitted)): ?>
    <div class="submitted"><?php print $submitted; ?></div>
  <?php endif; ?>

  <div class="content clear-block"><?php print $content; ?></div>

  <?php if (!empty($links)): ?>
    <div class="meta clear-block">

      <?php if (!empty($links)): ?>
        <div class="links"><?php print $links; ?></div>
      <?php endif; ?>

    </div> <!-- /meta -->
  <?php endif; ?>

</div> <!-- /node -->
