<div class="box">

  <?php if (!empty($title)): ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>

  <div class="content"><?php print $content; ?></div>

</div> <!-- /box -->
