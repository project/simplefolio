<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>">
<head>

  <title><?php print $head_title; ?></title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta http-equiv="Content-Style-Type" content="text/css" />
  <?php print $head; ?>
  <?php print $styles; ?>

</head>
<body class="<?php print $body_classes; ?>">

  <div id="page">
    <div id="header" class="clear-block">

      <div id="logo-title" class="clear-block">
        <?php if (!empty($logo)): ?>
          <a id="logo" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>

        <div id="title">
          <?php if (!empty($site_name)): ?>
            <h1 id="site-name">
              <a href="<?php print $front_page ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
            </h1>
          <?php endif; ?>

          <?php if (!empty($site_slogan)): ?>
            <div id="site-slogan"><?php print $site_slogan; ?></div>
          <?php endif; ?>
        </div> <!-- /title -->
      </div> <!-- /logo-title -->

      <?php if (!empty($header) || !empty($primary_links)): ?>
        <div id="header-right">
          <?php if (!empty($header)): ?>
            <div id="header-region" class="region clear-block"><?php print $header; ?></div>
          <?php endif; ?>

          <?php if (!empty($primary_links)): ?>
            <div id="primary" class="clear-block"><?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?></div>
          <?php endif; ?>
        </div> <!-- /header-right -->
      <?php endif; ?>

    </div> <!-- /header -->
    <div id="container" class="clear-block">
      <div id="content"><div id="content-inner">

        <?php if (!empty($front_top)): ?>
          <div id="front-top-region" class="region front clear-block"><?php print $front_top; ?></div>
        <?php endif; ?>

        <?php if (!empty($mission)): ?>
          <div id="mission"><?php print $mission; ?></div>
        <?php endif; ?>

        <?php if (!empty($front_left) || !empty($front_middle) || !empty($front_right)): ?>
          <div id="front" class="<?php if (!empty($front_class)) print $front_class .' '; ?>clear-block">

            <?php if (!empty($front_left)): ?>
              <div id="front-left-region" class="region front clear-block"><?php print $front_left; ?></div>
            <?php endif; ?>

            <?php if (!empty($front_middle)): ?>
              <div id="front-middle-region" class="region front clear-block"><?php print $front_middle; ?></div>
            <?php endif; ?>

            <?php if (!empty($front_right)): ?>
              <div id="front-right-region" class="region front clear-block"><?php print $front_right; ?></div>
            <?php endif; ?>

          </div> <!-- /front -->
        <?php endif; ?>

      </div></div> <!-- /content-inner /content -->
    </div> <!-- /container -->
    <div id="footer" class="clear-block">

      <?php if (!empty($footer)): ?>
        <div id="footer-region" class="region clear-block"><?php print $footer; ?></div>
      <?php endif; ?>

      <?php if (!empty($primary_links)): ?>
        <div id="primary-footer" class="clear-block"><?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?></div>
      <?php endif; ?>

      <?php if (!empty($footer_message)): ?>
        <div id="footer-message"><?php print $footer_message; ?></div>
      <?php endif; ?>

    </div> <!-- /footer -->
  </div> <!-- /page -->

  <?php print $scripts; ?>
  <?php print $closure; ?>

</body>
</html>
