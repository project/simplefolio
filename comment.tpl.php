<div class="comment<?php if ($comment->new) print ' comment-new'; ?><?php print ' '. $status; ?> <?php print $zebra; ?> clear-block">

  <?php if ($comment->new): ?>
    <span class="new"><?php print $new; ?></span>
  <?php endif; ?>

  <?php if (!empty($title)): ?>
    <h4><span class="author"><?php print $author; ?></span> says:<?php if (!empty($title)) print ' '. $title; ?></h4>
  <?php endif; ?>

  <?php if (!empty($submitted)): ?>
    <div class="submitted"><?php print $submitted; ?></div>
  <?php endif; ?>

  <div class="content"><?php print $content; ?></div>

  <?php print $links; ?>

</div> <!-- /comment -->
