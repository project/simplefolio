<?php
/**
 * Theme functions for SimpleFolio
 */


/**
 * Override template_preprocess_page().
 */
function simplefolio_preprocess_page(&$vars) {
  // Provide classes for non-empty regions
  if (!empty($vars['search_box']) || !empty($vars['secondary_links']) || !empty($vars['right'])) {
    $vars['content_class'] = 'right';
  }

  $front_regions = array('front_left', 'front_middle', 'front_right');
  $active_regions = array();
  foreach ($front_regions as $region) {
    if (!empty($vars[$region])) {
      $active_regions[] = $region;
    }
  }
  $vars['front_class'] = 'regions-'. count($active_regions);
}

/**
 * Override theme_node_submitted().
 */
function simplefolio_node_submitted($node) {
  if (module_exists('taxonomy')) {
    $terms = taxonomy_link('taxonomy terms', $node);

    if (!empty($terms)) {
      return t('Posted @datetime in !taxonomy by !username',
        array(
          '@datetime' => format_date($node->created, 'custom', 'jS F, Y'),
          '!taxonomy' => theme('links', $terms, array('class' => 'links terms-inline')),
          '!username' => theme('username', $node),
        )
      );
    }
  }

  return t('Posted @datetime by !username',
    array(
      '@datetime' => format_date($node->created, 'custom', 'jS F, Y'),
      '!username' => theme('username', $node),
    )
  );
}

/**
 * Override theme_comment_submitted().
 */
function simplefolio_comment_submitted($comment) {
  return t('@date at @time',
    array(
      '@date' => format_date($comment->timestamp, 'custom', 'jS F, Y'),
      '@time' => format_date($comment->timestamp, 'custom', 'g\:ia'),
    )
  );
}
