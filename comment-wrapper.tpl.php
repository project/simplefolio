<div id="comments">

  <h3><?php print format_plural($node->comment_count, '1 comment so far:', '@count comments so far:'); ?></h3>
  <?php print $content; ?>

</div>
